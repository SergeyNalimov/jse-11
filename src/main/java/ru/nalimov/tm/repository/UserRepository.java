package ru.nalimov.tm.repository;

import ru.nalimov.tm.entity.User;
import ru.nalimov.tm.util.HashMD5;
import ru.nalimov.tm.enumerated.Role;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;



public class UserRepository {

    private List<User> users = new ArrayList<>();

    public User create(
            final String login, final String userPassword, final String firstName,
            final String secondName, final String middleName
    ){
        final User user = new User(login, userPassword, firstName, secondName, middleName);
        users.add(user);
        return user;
    }

    public User create(
            final String login, final String userPassword, final String firstName, final String secondName,
            final String middleName, Role userRole
    ){
        final User user = new User(login, userPassword, firstName, secondName, middleName, userRole);
        users.add(user);
        return user;
    }

    public User findByLogin(final String login) {
        for (final User user: users) {
            if (user.getLogin().equals(login)) return user;
        }
        return null;
    }

    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        users.remove(user);
        return user;
    }

    public User updateByLogin(
            final String login, final String userPassword, final String firstName, final String lastName,
            final String middleName
    ){
        final User user = findByLogin(login);
        if (user == null) return null;
        user.setHashPassword(userPassword);
        user.setFirstName(firstName);
        user.setSecondName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    public User findById(final Long id) {
        if (id == null) return null;
        for (final User user: users) {
            if (user.getId().equals(id)) return user;
        }
        return null;
    }

    public User removeById(final Long id) {
        final User user = findById(id);
        if (user == null) return null;
        users.remove(user);
        return user;
    }

    public User updateById(
            final Long id, final String login, final String userPassword, final String firstName,
            final String lastName, final String middleName
    ) {
        final User user = findById(id);
        if (user == null) return null;
        user.setHashPassword(userPassword);
        user.setFirstName(firstName);
        user.setSecondName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    public void clear(){
        users.clear();
    }

    public int getSize() {
        return users.size();
    }

    public List<User> findAll()  {
        return users;
    }

}
