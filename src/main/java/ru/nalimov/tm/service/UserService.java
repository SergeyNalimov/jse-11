package ru.nalimov.tm.service;

import ru.nalimov.tm.entity.User;
import ru.nalimov.tm.repository.UserRepository;
import ru.nalimov.tm.enumerated.Role;
import java.util.List;
import java.util.ArrayList;

public class UserService {

    private List<User> users = new ArrayList<>();

    public UserService(UserRepository userRepository) {
    }

    public User create(
            final String login, final String userPassword, final String firstName,
            final String secondName, final String middleName
    ){
        final User user = new User(login, userPassword, firstName, secondName, middleName);
        users.add(user);
        return user;
    }

    public User create(
            final String login, final String userPassword, final String firstName, final String secondName,
            final String middleName, Role userRole
    ){
        final User user = new User(login, userPassword, firstName, secondName, middleName, userRole);
        users.add(user);
        return user;
    }

    public User findByLogin(final String login) {
        for (final User user: users) {
            if (user.getLogin().equals(login)) return user;
        }
        return null;
    }

    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        users.remove(user);
        return user;
    }

    public User updateByLogin(
            final String login, final String userPassword, final String firstName, final String secondName,
            final String middleName
    ){
        final User user = findByLogin(login);
        if (user == null) return null;
        user.setHashPassword(userPassword);
        user.setFirstName(firstName);
        user.setSecondName(secondName);
        user.setMiddleName(middleName);
        return user;
    }

    public User findById(final Long id) {
        if (id == null) return null;
        for (final User user: users) {
            if (user.getId().equals(id)) return user;
        }
        return null;
    }

    public User removeById(final Long id) {
        final User user = findById(id);
        if (user == null) return null;
        users.remove(user);
        return user;
    }

    public User updateById(
            final Long id, final String login, final String userPassword, final String firstName,
            final String secondName, final String middleName
    ) {
        final User user = findById(id);
        if (user == null) return null;
        user.setHashPassword(userPassword);
        user.setFirstName(firstName);
        user.setSecondName(secondName);
        user.setMiddleName(middleName);
        return user;
    }

    public void clear(){
        users.clear();
    }

    public int getSize() {
        return users.size();
    }

    public List<User> findAll()  {
        return users;
    }

}
