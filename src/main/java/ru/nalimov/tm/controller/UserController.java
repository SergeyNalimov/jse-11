package ru.nalimov.tm.controller;

import ru.nalimov.tm.entity.Project;
import ru.nalimov.tm.entity.User;
import ru.nalimov.tm.enumerated.Role;
import ru.nalimov.tm.service.UserService;

public class UserController extends AbstractController {

    private final UserService userService;

    public UserController(UserService userService) { this.userService = userService; }

    public int createUser() {
        System.out.println("[CREATE STANDART USER]");
        System.out.println("PLEASE, ENTER LOGIN");
        final String login = scanner.nextLine();
        System.out.println("PLEASE, ENTER PASSWORD");
        final String password = scanner.nextLine();
        System.out.println("PLEASE, ENTER FIRST NAME");
        final String firstName = scanner.nextLine();
        System.out.println("PLEASE, ENTER LAST NAME");
        final String secondName = scanner.nextLine();
        System.out.println("PLEASE, ENTER MIDDLE NAME");
        final String middleName = scanner.nextLine();
        userService.create(login, password, firstName, secondName, middleName);
        System.out.println("[OK]");
        return 0;
    }

    public int createUser(Role userRole) {
        System.out.println("[CREATE ADMIN]");
        System.out.println("PLEASE, ENTER LOGIN");
        final String login = scanner.nextLine();
        System.out.println("PLEASE, ENTER PASSWORD");
        final String password = scanner.nextLine();
        System.out.println("PLEASE, ENTER FIRST NAME");
        final String firstName = scanner.nextLine();
        System.out.println("PLEASE, ENTER LAST NAME");
        final String secondName = scanner.nextLine();
        System.out.println("PLEASE, ENTER MIDDLE NAME");
        final String middleName = scanner.nextLine();
        userService.create(login, password, firstName, secondName, middleName, userRole);
        System.out.println("[OK]");
        return 0;
    }

    public int updateUserByLogin(){
        System.out.println("[UPDATE USER]");
        System.out.println("PLEASE, ENTER LOGIN");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        if (user == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE, ENTER PASSWORD");
        final String password = scanner.nextLine();
        System.out.println("PLEASE, ENTER FIRST NAME");
        final String firstName = scanner.nextLine();
        System.out.println("PLEASE, ENTER LAST NAME");
        final String secondName = scanner.nextLine();
        System.out.println("PLEASE, ENTER MIDDLE NAME");
        final String middleName = scanner.nextLine();
        userService.updateByLogin(login, password, firstName, secondName, middleName);
        System.out.println("[OK]");
        return 0;
    }

    public int clearUsers() {
        System.out.println("[CLEAR USERS]");
        userService.clear();
        System.out.println("[OK]");
        return 0;
    }

    public int removeUserByLogin() {
        System.out.println("[REMOVE USER BY LOGIN]");
        System.out.println("PLEASE, ENTER LOGIN");
        final String login = scanner.nextLine();
        final User user = userService.removeByLogin(login);
        if (user == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int listUsers() {
        System.out.println("[LIST USERS]");
        int index = 1;
        for (final User user: userService.findAll()) {
            System.out.println(index + ". " + user.getId() + " "  + user.getLogin() + ": " + user.getHashPassword() + " " + user.getUserRole());
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }

    public void viewUser(final User user) {
        if (user == null) return;
        System.out.println("[VIEW USER DETAIL]");
        System.out.println("USER ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("PASSWORD HASH: " + user.getHashPassword());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("LAST NAME: " + user.getSecondName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("ROLE: " + user.getUserRole());
        System.out.println("[OK]");
    }

    public int viewUserByLogin() {
        System.out.println("ENTER LOGIN:");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        if (user == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        this.viewUser(user);
        return 0;
    }

    public int viewUserById() {
        System.out.println("PLEASE, ENTER USER ID:");
        final long id = scanner.nextLong();
        final User user = userService.findById(id);
        if (user == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        this.viewUser(user);
        return 0;
    }

    public int updateUserById(){
        System.out.println("[UPDATE USER]");
        System.out.println("PLEASE, ENTER USER ID:");
        final long id = scanner.nextLong();
        final User user = userService.findById(id);
        if (user == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE, ENTER LOGIN:");
        final String login = scanner.nextLine();
        System.out.println("PLEASE, ENTER USER ID:");
        final String password = scanner.nextLine();
        System.out.println("PLEASE, ENTER FIRST NAME");
        final String firstName = scanner.nextLine();
        System.out.println("PLEASE, ENTER LAST NAME");
        final String secondName = scanner.nextLine();
        System.out.println("PLEASE, ENTER MIDDLE NAME");
        final String middleName = scanner.nextLine();
        userService.updateById(id, login, password, firstName, secondName, middleName);
        System.out.println("[OK]");
        return 0;
    }

    public int removeUserById() {
        System.out.println("[REMOVE USER BY ID]");
        System.out.println("PLEASE, ENTER USER ID:");
        final long id = scanner.nextLong();
        final User user = userService.removeById(id);
        if (user == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

}
