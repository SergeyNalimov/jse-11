package ru.nalimov.tm.entity;

import ru.nalimov.tm.enumerated.Role;
import ru.nalimov.tm.util.HashMD5;

public class User {

    private Long id = System.nanoTime();

    private String login = "";

    private String hashPassword = "";

    private String firstName = "";

    private String secondName = "";

    private String middleName = "";

    private Role userRole;

    public User(){}

    public User(final String login, final String userPassword, final String firstName, final String secondName, final String middleName, final Role userRole) {
        this.login = login;
        this.hashPassword = HashMD5.getHash(userPassword);
        this.firstName = firstName;
        this.secondName = secondName;
        this.middleName = middleName;
        this.userRole = userRole;
    }

    public User(final String login, final String userPassword, final String firstName, final String secondName, final String middleName) {
        this.login = login;
        this.hashPassword = HashMD5.getHash(userPassword);
        this.firstName = firstName;
        this.secondName = secondName;
        this.middleName = middleName;
        this.userRole = Role.USER;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getHashPassword() {
        return hashPassword;
    }

    public void setHashPassword(String password){
        this.hashPassword = HashMD5.getHash(hashPassword);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Role getUserRole() {
        return userRole;
    }

    public void setUserRole(Role userRole) {
        this.userRole = userRole;
    }

}
